# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import contract


def register():
    Pool.register(
        contract.ContractProductLine,
        contract.AnalyticAccountEntry,
        contract.SaleContractLine,
        module='analytic_sale_contract', type_='model')
