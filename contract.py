# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.analytic_account import AnalyticMixin
from trytond.pyson import Eval


STATES = {
        'readonly': Eval('state') != 'draft',
}


class SaleContractLine(metaclass=PoolMeta):
    __name__ = 'sale.contract.line'

    @classmethod
    def __setup__(cls):
        super(SaleContractLine, cls).__setup__()

    def get_sale_line(self, contract, line):
        value = super(SaleContractLine, self).get_sale_line(contract, line)
        if line.analytic_account:
            value['analytic_accounts'] = [('create', [{
                'account': line.analytic_account,
                'root': line.analytic_account.root.id
            }])]
        return value


class ContractProductLine(metaclass=PoolMeta):
    __name__ = "sale.contract.product_line"
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
        ])

    @classmethod
    def __setup__(cls):
        super(ContractProductLine, cls).__setup__()


class AnalyticAccountEntry(metaclass=PoolMeta):
    __name__ = 'analytic.account.entry'

    @classmethod
    def _get_origin(cls):
        origins = super(AnalyticAccountEntry, cls)._get_origin()
        return origins + ['sale.contract.product_line']
